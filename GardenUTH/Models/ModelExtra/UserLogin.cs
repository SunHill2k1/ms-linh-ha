using System.ComponentModel.DataAnnotations;

namespace GardenUTH.Models.ModelExtra;

public class UserLogin
{
    [Required]
    public string name { get; set; }
    [Required]
    [DataType(DataType.Password)]
    public string password { get; set; }

}