using System.ComponentModel;
using System.Net;
using System.Runtime.Serialization;

namespace GardenUTH.Models.ModelExtra;

[DataContract]
public class ResponseWrapper
{
    [DataMember] [DefaultValue(false)] public bool Success { get; set; }

    [DataMember] public int StatusCode { get; set; }

    [DataMember(EmitDefaultValue = false)] public object Data { get; set; }

    [DataMember(EmitDefaultValue = false)] public string Message { get; set; }

    public ResponseWrapper(HttpStatusCode statusCode, object data = null,
        string message = null)
    {
        this.StatusCode = (int)statusCode;
        this.Success = this.StatusCode == 200 ||
                       this.StatusCode == 201 ||
                       this.StatusCode == 204;
        this.Data = data;
        this.Message = message;
    }


    public ResponseWrapper(HttpStatusCode statusCode, string message) :
        this(statusCode, null, message)
    {
    }
}