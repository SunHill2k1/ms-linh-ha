﻿using System.ComponentModel;

namespace GardenUTH.Models.ModelExtra
{
    public class GetModelRequest
    {
        [DefaultValue(-1)]
        public List<int>? IdType { get; set; }
        [DefaultValue("Price")]
        public string? OrderBy { get; set; }
        [DefaultValue(true)]
        public bool? AES { get; set; }

        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
       
        public string? Search { get; set; }
        [DefaultValue(-1)]
        public int? PriceFrom { get; set; }
        [DefaultValue(-1)]

        public int? PriceTo { get; set; }

    }
    
    public class PagingUserRequest
    {
        // [DefaultValue("*")]
        public List<string>? Role { get; set; }
        // [DefaultValue("*")]
        public List<string>? Belong { get; set; }
        [DefaultValue("Role")]
        public string? OrderBy { get; set; }
        [DefaultValue(true)]
         public bool? AES { get; set; }
         [DefaultValue(0)]
        public int? PageIndex { get; set; }
        [DefaultValue(10)]
        public int? PageSize { get; set; }
       
        public string? Search { get; set; }

    }
    public class PagingBillRequest
    {
        
        public List<int?>? CreateBy { get; set; }
       
        public List<string>? CustomerName { get; set; }
        [DefaultValue("DateUsed")]
        public string? OrderBy { get; set; }
        [DefaultValue(false)]
        public bool? AES { get; set; }
        [DefaultValue(0)]
        public int? PageIndex { get; set; }
        [DefaultValue(10)]
        public int? PageSize { get; set; }
       
        public string? Search { get; set; }

    }
    public class PagingSpendingRequest
    {
        //public List<int?>? CreateBy { get; set; }
        [DefaultValue(0)]
        public int? PageIndex { get; set; }
        [DefaultValue(10)]
        public int? PageSize { get; set; }
       
        public string? Search { get; set; }

    }
    public class PagingVoucherRequest
    {
        //
        // public List<int?>? CreateBy { get; set; }
        // [DefaultValue("DateUsed")]
       // public string? OrderBy { get; set; }
        public string? Status { get; set; } 

        [DefaultValue(0)]
        public int? PageIndex { get; set; }
        [DefaultValue(10)]
        public int? PageSize { get; set; }
       
        public string? Search { get; set; }

    }
    public class IncomeRequest
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public bool? Shift { get; set; } 
        public string? Belong { get; set; }

    }
    
}
