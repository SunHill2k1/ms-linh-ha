namespace GardenUTH.Models.ModelExtra;

public class ChangePassViewModel
{
    public string OldPassword { get; set; }
    public string NewPassword { get; set; }
    public string ConfirmNewPassword { get; set; }

}