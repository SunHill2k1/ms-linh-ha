using System.Runtime.Serialization;
using Microsoft.VisualBasic;

namespace GardenUTH.Models.ModelExtra;

public class LoginViewModel
{
    public string? Token { get; set; }
     public string? TokenType { get; set; }
    // public string RefreshToken { get; set; } 
    public string Fullname { get; set; }
    
    // public DateTime Timestamp { get; set; }

    public string Role { get; set; }

    public int IdUser { get; set; }
    // public string? Address { get; set; }

    public string? SDT { get; set; }
     public bool? Shift { get; set; }
    public string? Belong { get; set; }

}