﻿namespace GardenUTH.Models.ModelExtra
{
    public class PageResult<T>
    {
        public List<T>? Data { get; set; }
        public int TotalRecord { get; set; }
        public int? PageSize { get; set; }
        public int? PageIndex { get; set; }
        public int? TotalPages { get; set; }

    }

    // public class PageResult
    // {
    //     public dynamic? Data { get; set; }
    //     public dynamic TotalRecord { get; set; }
    // }
}
