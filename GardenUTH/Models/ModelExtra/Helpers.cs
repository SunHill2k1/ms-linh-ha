namespace GardenUTH.Models;

public class Helpers
{
    public static string AppName { get; set; }
    public static void LogMessage(Exception e)
    {
        Serilog.Log.Error(e, $"{AppName}_{e.Message}");
    }
}