namespace GardenUTH.Models.ModelExtra;

public class AuthConstants
{
    public const string ID = "IdUser";

    public const string FULL_NAME = "FullName";

    public const string ROLE = "Role";     
    public const string TOKEN = "Token";      
    public const string BELONG = "Belong";      
    
}