﻿using System.ComponentModel.DataAnnotations;

namespace GardenUTH.Models.ModelDto;

public class UserRegister
{
    [Required]

    public string Name { get; set; }
    
    public string? Role { get; set; }
    
    public string? Address { get; set; }
    public string? PhoneNumber { get; set; }

    [Required]
    [EmailAddress]
    [Display(Name = "Email")]
    public string Email { get; set; }

    [Required]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; set; }

    
}

public class UserRegisterAdmin
{
    [Required]
    public string Name { get; set; }
    public string? FullName { get; set; }
    public string? Address { get; set; }
    public string? PhoneNumber { get; set; }
    [Required]
    [EmailAddress]
    [Display(Name = "Email")]
    public string Email { get; set; }
    [Required]
    [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; set; }
    public string Role     { get; set; }
}