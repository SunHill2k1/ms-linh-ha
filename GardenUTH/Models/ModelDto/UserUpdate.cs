﻿namespace GardenUTH.Models.ModelDto;

public class UserUpdate
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? SDT { get; set; }
    public string? Belong { get; set; }
    public string? Role { get; set; }
    public bool? Shift { get; set; }


}