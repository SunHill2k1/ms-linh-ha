using GardenUTH.Entities;

namespace GardenUTH.Models.ModelDto;

public class BIllResponse
{
    public Bill? Bill { get; set; }
    public string? Belong { get; set; }
}