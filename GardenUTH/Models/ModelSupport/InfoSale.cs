namespace GardenUTH.Models.ModelSupport;

public class InfoSale
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Account { get; set; }
    public int? TotalVoucher { get; set; }
}