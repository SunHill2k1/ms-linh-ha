﻿using System.Net;
using System.Text;
using GardenUTH.Models;
using GardenUTH.Models.ModelExtra;
using Microsoft.AspNetCore.Mvc;

namespace GardenUTH.Controllers.ControllerExtension
{

    // [EnableCors("PolicyAll")]
    [Route("v1/[controller]")]
    [ApiController]
    //[Authorize]
    public class BaseApiController<T> : ControllerBase
    {
        protected readonly T _service;

        public BaseApiController(T service)
        {
            _service = service;
        }

        protected ResponseWrapper Error(HttpStatusCode code, string msg)
        {
            HttpContext.Response.StatusCode = (int)code;
            return new ResponseWrapper(code, msg);
        }

        protected ResponseWrapper Error(string msg)
        {
            HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return new ResponseWrapper(HttpStatusCode.BadRequest, msg);
        }

        protected ResponseWrapper Ok_Get(object data,string ms="Thành công")
        {
            Response.StatusCode = (int)HttpStatusCode.OK;
            return new ResponseWrapper(HttpStatusCode.OK,data, ms);
        }

        protected ResponseWrapper Ok_Create(object data, string ms="Tạo thành công")
        {
            //Response.StatusCode = (int)HttpStatusCode.Created;
            return new ResponseWrapper(HttpStatusCode.Created, data,  ms);
        }

        protected ResponseWrapper Ok_Create()
        {
            //Response.StatusCode = (int)HttpStatusCode.Created;
            return new ResponseWrapper(HttpStatusCode.Created, null);
        }

        protected ResponseWrapper Ok_Update(object data,string ms = "Update success")
        {
            return Ok_Get(data,ms);
        }

        protected ResponseWrapper Ok_Delete(object data = null,string ms="Đã xóa")
        {
            return new ResponseWrapper(HttpStatusCode.NoContent,data ,ms);

        }

        protected ResponseWrapper NotFound()
        {
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return new ResponseWrapper(HttpStatusCode.NotFound, null,"Not found");
        }

        protected ResponseWrapper NotFound(string msg)
        {
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return new ResponseWrapper(HttpStatusCode.NotFound,null, msg);
        }
        protected ResponseWrapper NoContent(string msg)
        {
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            return new ResponseWrapper(HttpStatusCode.NoContent, msg);
        }

        protected ResponseWrapper BadRequest(string msg)
        {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return new ResponseWrapper(HttpStatusCode.BadRequest, msg);
        }

        protected string GetModelStateErrMsg()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var value in ModelState.Values)
            {
                foreach (var error in value.Errors)
                {
                    stringBuilder.Append(error.ErrorMessage);
                }
            }

            return stringBuilder.ToString();
        }

       
    }
}
