using AutoMapper;
using AutoMapper.QueryableExtensions;
using GardenUTH.Context;
using GardenUTH.Entities;
using GardenUTH.Models;
using GardenUTH.Models.ModelExtra;
using GardenUTH.Models.ModelSupport;
using GardenUTH.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GardenUTH.Controllers.ControllersCore;
[Route("api/[controller]")]
[ApiController]
public class VoucherController: ControllerBase
{
    
    
        private readonly MyDbContext _context;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        // private readonly IUserService _userService;


        public VoucherController(MyDbContext context, IUserService userService, IMapper mapper) : base(
        )
        {
            _context = context;
            _userService = userService;
            _mapper = mapper;
        }
        [HttpGet("GetListVoucher")]
        [Authorize]
        public async Task<PageResult<Voucher>> GetListVoucher([FromQuery] PagingVoucherRequest request)
        {
            try
            {
                var nextPage =  _context.Vouchers
                    .Include(voucher => voucher.CreateByNavigation)
                    .Where(x => request.Status == null || x.Status == request.Status )
                    .Where(x => request.Search == null || x.Value!.Contains(request.Search)  )
                    .OrderBy(x=> x.CreateDate)
                    .ProjectTo<Voucher>(_mapper.ConfigurationProvider, x => x.CreateByNavigation);
        
                var res = nextPage
                    .Skip(((int)request.PageIndex!) * (int)request.PageSize!)
                    .Take((int)request.PageSize)
                    .ToList();
                return new PageResult<Voucher>()
                {
                    TotalRecord = nextPage.Count(),
                    PageSize = request.PageSize,
                    PageIndex = request.PageIndex,
                    TotalPages = nextPage.Count() / request.PageSize + 1,
                    Data = res
                };
            }
            catch (Exception e)
            {
                Helpers.LogMessage(e);
                throw;
            }
        }

//[FromQuery] GetModelRequest request
        [HttpGet("GetListVoucher2")]
        [Authorize]
        public async Task<IQueryable<VoucherResponse>> GetListVoucher()
        {
            try
            {
                var result = _context.Vouchers
                    .Join(
                        _context.Admins,
                        voucher => voucher.CreateBy,
                        admin => admin.Id,
                        (voucher, admin) => new { Voucher = voucher, Admin = admin }
                        
                    )
                        .Where(x => x.Voucher.Status == "đã sử dụng")
                    .Select(group => new VoucherResponse
                    {
                        Id=group.Voucher.Id,
                        Value = group.Voucher.Value,
                        CreateDate = group.Voucher.CreateDate ,
                        CreateBy = group.Voucher.CreateBy,
                        Status = group.Voucher.Status,
                        CreateName = group.Admin.Name

                    });
                return result;
            }
            catch (Exception e)
            {
                Helpers.LogMessage(e);
                throw e;
            }
        }
        
        [HttpPost("GenVoucher")]
        [Authorize]
        public async Task<Voucher> GenVoucher()
        {
            var random = new Random();
            const string chars = "0123456789";
            var temp = new string(Enumerable.Repeat(chars, 6).Select(s => s[random.Next(s.Length)]).ToArray());
            var newValue = _userService.GetUserBelong()+" "+ temp ;
            var newVa = new Voucher()
            {
                Value = newValue,
               // Expired = DateTime.Now.AddDays(15),
                Status="có sẵn",
                CreateBy=_userService.GetUserId(),
                CreateDate=DateTime.Now
            };
            try
            {
                
                _context.Vouchers.Add(newVa);
                await _context.SaveChangesAsync();
                return newVa;
            }
            catch (Exception e)
            {
                Helpers.LogMessage(e);
                throw e;
            }
        }
        
        [HttpGet("GetInfoSaler")]
        public async Task<InfoSale?> GetInfoSaler(int createby)
        {
            try
            {
                //var user1 = _context.Admins.FindAsync(createby);
                var result =await _context.Vouchers.Join(
                        _context.Admins,
                        voucher => voucher.CreateBy,
                        admin => admin.Id,
                        (voucher, admin) => new { Voucher = voucher, Admin = admin }
                    )
                    .Where(x => x.Voucher.CreateBy == createby)
                    .GroupBy(x => new { x.Admin.Name, x.Admin.Account, x.Voucher.CreateBy })
                    .Select(group => new InfoSale
                    {
                        TotalVoucher = group.Count(),
                        Id = createby,
                        Account = group.Key.Account,
                        Name = group.Key.Name
                    })
                    .FirstOrDefaultAsync();;
                return result;
            }
            catch (Exception e)
            {
                Helpers.LogMessage(e);
                throw e;
            }
        }
    
}