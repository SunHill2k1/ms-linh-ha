using GardenUTH.Context;
using GardenUTH.Controllers.ControllerExtension;
using GardenUTH.Entities;
using GardenUTH.Models.ModelExtra;
using GardenUTH.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GardenUTH.Controllers.ControllersCore
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpendingController : BaseApiController<ISpendingServer>
    {
        private readonly MyDbContext _context;
        private readonly ISpendingServer _spendingService;
        private readonly IUserService _userService;


        public SpendingController(MyDbContext context, ISpendingServer spendingService, IUserService userService) : base(
            spendingService)
        {
            _context = context;
            _spendingService = spendingService;
            _userService = userService;
        }

        // GET: api/Order
        [HttpPost("PostSpending")]
        [Authorize]
        public async Task<ResponseWrapper> PostSpending(Spending request)
        {
            try
            {
                var result = await _spendingService.PostSpending(request);
                return Ok_Create(result, "Tạo chi tiêu thành công");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        // GET: api/Order/5
        [HttpGet("GetSpending")]
        [Authorize]
        public async Task<ResponseWrapper> GetSpending([FromQuery] PagingSpendingRequest request)
        {
            try
            {
                var result = await _spendingService.GetSpending(request);
                return Ok_Get(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpGet("Spending")]
        [Authorize]
        public async Task<ResponseWrapper> Spending([FromQuery] IncomeRequest request)
        {
            try
            {
                var result = await _spendingService.Spending(request);
                return Ok_Get(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
        //
        //

    }
}
