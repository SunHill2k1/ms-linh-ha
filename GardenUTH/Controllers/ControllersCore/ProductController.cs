using AutoMapper;
using AutoMapper.QueryableExtensions;
using GardenUTH.Context;
using GardenUTH.Controllers.ControllerExtension;
using GardenUTH.Entities;
using GardenUTH.Models;
using GardenUTH.Models.ModelDto;
using GardenUTH.Models.ModelExtra;
using GardenUTH.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace GardenUTH.Controllers.ControllersCore
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly MyDbContext _context;
        private readonly IMapper _mapper;


        public ProductController(MyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            
        }
//[FromQuery] GetModelRequest request
        [HttpGet("GetListVoucher")]
        public async Task<PageResult<Voucher>> GetListVoucher([FromQuery] PagingVoucherRequest request)
        {
            try
            {
                var nextPage =  _context.Vouchers
                     .Include(voucher => voucher.CreateByNavigation)
                    .Where(x => request.Status == null || x.Status == request.Status )
                     .Where(x => request.Search == null || x.Value!.Contains(request.Search)  )
                     .OrderBy(x=> x.CreateDate)
                    .ProjectTo<Voucher>(_mapper.ConfigurationProvider, x => x.CreateByNavigation);
        
                var res = nextPage
                    .Skip(((int)request.PageIndex!) * (int)request.PageSize!)
                    .Take((int)request.PageSize)
                    .ToList();
                return new PageResult<Voucher>()
                {
                    TotalRecord = nextPage.Count(),
                    PageSize = request.PageSize,
                    PageIndex = request.PageIndex,
                    TotalPages = nextPage.Count() / request.PageSize + 1,
                    Data = res
                };
            }
            catch (Exception e)
            {
                Helpers.LogMessage(e);
                throw;
            }
        }
    }
}
