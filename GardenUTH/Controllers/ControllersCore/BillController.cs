using GardenUTH.Context;
using GardenUTH.Controllers.ControllerExtension;
using GardenUTH.Entities;
using GardenUTH.Models.ModelExtra;
using GardenUTH.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GardenUTH.Controllers.ControllersCore
{
    [Route("api/[controller]")]
    [ApiController]
    public class BillController : BaseApiController<IBillService>
    {
        private readonly MyDbContext _context;
        private readonly IBillService _billService;
        private readonly IUserService _userService;


        public BillController(MyDbContext context, IBillService billService, IUserService userService) : base(
            billService)
        {
            _context = context;
            _billService = billService;
            _userService = userService;
        }

        // GET: api/Order
        [HttpPost("PostBill")]
        [Authorize]
        public async Task<ResponseWrapper> PostBill(Bill request)
        {
            try
            {
                var result = await _billService.PostBill(request);
                return Ok_Create(result, "Thanh toán thành công");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        // GET: api/Order/5
        [HttpGet("GetAllBill")]
        [Authorize]
        public async Task<ResponseWrapper> GetBill([FromQuery] PagingBillRequest request)
        {
            try
            {
                var result = await _billService.GetBill(request);
                return Ok_Get(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpGet("GetInCome")]
        [Authorize]
        public async Task<ResponseWrapper> Income([FromQuery] IncomeRequest request)
        {
            try
            {
                var result = await _billService.Income(request);
                return Ok_Get(result);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        // [HttpGet("GetAllOrderList")]
        //  [Authorize(Roles = "Admin,Shipper")]
        // // [Authorize(Roles = "Shipper")]
        //
        // public async Task<ResponseWrapper> GetAllOrderList([FromQuery] GetAllOrderList request)
        // {
        //     try
        //     {
        //         var allListOrder = await _orderService.GetAllOrderList(request);
        //         return Ok_Get(allListOrder);
        //     }
        //     catch (Exception e)
        //
        //     {
        //         return NotFound(e.Message);
        //     }
        // }
        //
        // [HttpGet("GetChartOrder")]
        // [Authorize(Roles = "Admin")]
        // public  ResponseWrapper StatisticalOrder()
        // {
        //     try
        //     {
        //         var result =  _orderService.StatisticalOrder();
        //         return Ok_Get(result);
        //     }
        //     catch (Exception e)
        //
        //     {
        //         return NotFound(e.Message);
        //     }
        // }
        //
        // [HttpGet("GetChartTypeProduct")]
        // [Authorize(Roles = "Admin")]
        // public  ResponseWrapper GetChartTypeProduct(int idTypeProduct)
        // {
        //     try
        //     {
        //         var result =  _orderService.GetChartTypeProduct(idTypeProduct);
        //         return Ok_Get(result);
        //     }
        //     catch (Exception e)
        //
        //     {
        //         return NotFound(e.Message);
        //     }
        // }
        // [HttpGet("GetReportProduct")]
        // [Authorize(Roles = "Admin")]
        // public async Task<ResponseWrapper> GetReportProduct(int month)
        // {
        //     try
        //     {
        //         var result =await  _orderService.GetReportProduct(month);
        //         return Ok_Get(result);
        //     }
        //     catch (Exception e)
        //
        //     {
        //         return NotFound(e.Message);
        //     }
        // }
        //
        // [HttpGet("GetReportTypeProduct")]
        // //[Authorize(Roles = "Admin")]
        // public   ResponseWrapper GetReportTypeProduct()
        // {
        //     try
        //     {
        //         var result =  _orderService.GetReportTypeProduct();
        //         return Ok_Get(result);
        //     }
        //     catch (Exception e)
        //
        //     {
        //         return NotFound(e.Message);
        //     }
        // }
        //
        // [HttpPost("ConfirmOrder")]
        // [Authorize(Roles = "Admin")]
        // public async Task<ResponseWrapper> ConfirmOrder(ConfirmOrder request)
        // {
        //     try
        //     {
        //         var allListOrder = await _orderService.ConfirmOrder(request.IdOrder, request.IsConfirm);
        //         return Ok_Get(allListOrder);
        //     }
        //     catch (Exception e)
        //     {
        //         return NotFound(e.Message);
        //     }
        // }
        //
        // [HttpPost("HandlerOrderProduct")]
        // [Authorize(Roles = "Shipper")]
        // public async Task<ResponseWrapper> HandlerOrderProduct(int idOrder)
        // {
        //     try
        //     {
        //         var result = await _orderService.HandlerOrderProduct(idOrder);
        //         return Ok_Get(result);
        //     }
        //     catch (Exception e)
        //     {
        //         return Error(e.Message);
        //     }
        // }
        //
        // [HttpPost("DoneOrderProduct")]
        // [Authorize(Roles = "Shipper")]
        // public async Task<ResponseWrapper> DoneOrderProduct(int idOrder)
        // {
        //     try
        //     {
        //         var result = await _orderService.DoneOrderProduct(idOrder);
        //         return Ok_Get(result);
        //     }
        //     catch (Exception e)
        //     {
        //         return Error(e.Message);
        //     }
        // }
        //
        // [HttpPost("CancelOrder")]
        // [Authorize(Roles = "Buyer,Admin")]
        // public async Task<ResponseWrapper> CancelOrder(int idOrder)
        // {
        //     try
        //     {
        //         var result = await _orderService.CancelOrder(idOrder);
        //         return Ok_Get(result);
        //     }
        //     catch (Exception e)
        //     {
        //         return Error(e.Message);
        //     }
        // }
        //
        // [HttpGet("GetOrderDetail")]
        // [Authorize]
        //
        // public async Task<ResponseWrapper> GetOrderDetail(int idOrder)
        // {
        //     try
        //     {
        //         var listOrderDetail = _context.OrderDetails.Where(o => o.IdOrder == idOrder)
        //             .Include(o => o.IdProductNavigation)
        //             .Include(o => o.IdOrderNavigation)
        //             .Include(o => o.IdOrderNavigation.IdUserNavigation)
        //             .ToList();
        //             // .Include("Name")
        //             // .Include("Amount");
        //         return Ok_Get(listOrderDetail);
        //     }
        //     catch (Exception e)
        //     {
        //         return NotFound(e.Message);
        //     }
        // }
        //
        // // PUT: api/Order/5
        // // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        // // [HttpPut("{id}")]
        // // public async Task<IActionResult> PutOrder(int id, Order order)
        // // {
        // //     if (id != order.IdOrder)
        // //     {
        // //         return BadRequest();
        // //     }
        // //
        // //     _context.Entry(order).State = EntityState.Modified;
        // //
        // //     try
        // //     {
        // //         await _context.SaveChangesAsync();
        // //     }
        // //     catch (DbUpdateConcurrencyException)
        // //     {
        // //         if (!OrderExists(id))
        // //         {
        // //             return NotFound();
        // //         }
        // //         else
        // //         {
        // //             throw;
        // //         }
        // //     }
        // //
        // //     return NoContent();
        // // }
        //
        // // POST: api/Order
        // // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //
        //
        // // DELETE: api/Order/5
        // [HttpDelete("{id}")]
        // [Authorize(Roles = "Admin")]
        //
        // public async Task<ResponseWrapper> DeleteOrder(int id)
        // {
        //     var order = await _context.Orders.FindAsync(id);
        //     _context.Orders.Remove(order);
        //     await _context.SaveChangesAsync();
        //
        //     return Ok_Delete();
        // }

    }
}
