using GardenUTH.Context;
using GardenUTH.Controllers.ControllerExtension;
using GardenUTH.Entities;
using GardenUTH.Models.ModelDto;
using GardenUTH.Models.ModelExtra;
using GardenUTH.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GardenUTH.Controllers.ControllersCore;
[Route("api/[controller]")]
[ApiController]

public class UserController: BaseApiController<IUserService>
{
    private readonly IUserService _useservice;
    private readonly MyDbContext _context;

    public UserController(MyDbContext context, IUserService useservice) : base(useservice)
    {
        _context = context;
        _useservice = useservice;
    }
    [HttpPost("Login")]
    public async Task<ResponseWrapper> Login( UserLogin request)
    {
        try
        {
            var result = await _useservice.Login(request);
            //SetJWTCookie(result.Token);
            return Ok_Get(result, "Thành công");
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }
    [HttpPost("GetBelong")]
    [Authorize]
    public async Task<string> GetBelong()
    {
        try
        {
            var result =  _useservice.GetUserBelong();
            //SetJWTCookie(result.Token);
            return result;
        }
        catch (Exception e)
        {
            throw e;
        }
    }
    [HttpPost("Register")]
    [Authorize]
    public async Task<ResponseWrapper> PostUserInfo(Admin userInfo)
    {
        try
        {
            var result = await _service.PostUserInfo(userInfo);
            return Ok_Create(result, "Tạo tài khoản thành công");
        }
        catch (Exception e)
        {
            return Error("Tạo thất bại vì " + e.Message);
        }
    }
    [HttpGet("GetAllUser")]
    [Authorize]
    public async Task<ResponseWrapper> GetAllUser([FromQuery] PagingUserRequest request)
    {
        try
        {
            var result =await _useservice.GetAllUser(request);
            return Ok_Get(result);
        }
        catch (Exception e)
        {
            return NotFound(e.Message);
        }
    }
    [Authorize]
    [HttpPut("ChangePassword")]
    public async Task<ResponseWrapper> ChangePassword(ChangePassViewModel request)
    {
        try
        {
            var result = await _useservice.ChangePassword(request);
            return Ok_Update(result, "Đổi mật khẩu thành công");
        }
        catch (Exception e)
        {
            return Error("Đổi mật khẩu thất bại vì " + e.Message);
        }
    }
    [HttpPatch("UpdateUser")]
    [Authorize(Roles="Admin")]
    public async Task<ResponseWrapper> UpdateUser(UserUpdate userInfo)
    {
         
        try
        {
            var result = await _useservice.UpdateUser(userInfo);
            return Ok_Update(result, "Cập nhật User thành công");
        }
        catch (Exception e)
        {
            return Error("Cập nhật User thất bại  vì " + e.Message);
        }
    }
    [HttpPatch("UpdateProfile")]
    [Authorize]
    public async Task<ResponseWrapper> UpdateProfile(UserUpdate userInfo)
    {
         
        try
        {
            var result = await _useservice.UpdateProfile(userInfo);
            return Ok_Update(result, "Cập nhật User thành công");
        }
        catch (Exception e)
        {
            return Error("Cập nhật User thất bại  vì " + e.Message);
        }
    }
    
    [HttpDelete("DeleteUser")]
    [Authorize(Roles="Admin")]
    public async Task<ResponseWrapper> DeleteUser(int id)
    {
         
        try
        {
            var result = await _useservice.RemoveUser(id);
            return Ok_Update(result, "Xóa User thành công");
        }
        catch (Exception e)
        {
            return Error("XóaUser thất bại  vì " + e.Message);
        }
    }
    
    [HttpGet("me")]
    [Authorize()]
    public async Task<ResponseWrapper> GetInforUser()
    {
         
        try
        {
            var result = await _useservice.GetInforUser();
            return Ok_Update(result);
        }
        catch (Exception e)
        {
            return Error(e.Message);
        }
    }






}