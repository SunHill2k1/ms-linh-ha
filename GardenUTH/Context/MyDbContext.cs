using GardenUTH.Entities;
using Microsoft.EntityFrameworkCore;

namespace GardenUTH.Context
{
    public partial class MyDbContext : DbContext
    {
        public MyDbContext()
        {
        }

        public MyDbContext(DbContextOptions<MyDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admins { get; set; } = null!;
        public virtual DbSet<Voucher> Vouchers { get; set; } = null!;
        public virtual DbSet<Bill> Bills { get; set; } = null!;
        public virtual DbSet<Spending> Spendings { get; set; } = null!;


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=ConnectionStrings:devDb");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.ToTable("Admin");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Shift).HasColumnName("shift");


                entity.Property(e => e.Account)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("account");

                entity.Property(e => e.Belong)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasColumnName("belong");

                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("password");

                entity.Property(e => e.Role)
                    .HasMaxLength(100)
                    .HasColumnName("role");
                entity.Property(e => e.SDT)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("sdt");
                entity.Property(e => e.IsDeleted)
                    .HasColumnName("isdeleted");
            });

            modelBuilder.Entity<Voucher>(entity =>
            {
                entity.ToTable("Voucher");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasColumnName("create_Date");

                // entity.Property(e => e.Expired)
                //     .HasColumnType("datetime")
                //     .HasColumnName("expired");
                //
                // entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .HasColumnName("status");

                entity.Property(e => e.Value)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("value");

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Vouchers)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("Voucher_Admin_null_fk");
            });
            
            modelBuilder.Entity<Bill>(entity =>
            {
                entity.ToTable("Bill");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CodeKtv).HasColumnName("code_KTV");
                entity.Property(e => e.Room).HasColumnName("room");

                // entity.Property(e => e.CreateDate)
                //     .HasColumnType("datetime")
                //     .HasColumnName("create_date");

                entity.Property(e => e.CustomerGroup).HasColumnName("customer_group");

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(40)
                    .HasColumnName("customer_name");

                entity.Property(e => e.CustomerOdd).HasColumnName("customer_odd");
                entity.Property(e => e.CustomerVoucher).HasColumnName("customer_voucher");


                entity.Property(e => e.DateUsed)
                    .HasColumnType("datetime")
                    .HasColumnName("date_used");

                entity.Property(e => e.NotedCustomer)
                    .HasMaxLength(100)
                    .HasColumnName("noted_customer");

                entity.Property(e => e.NotedVoucher).HasColumnName("noted_voucher");

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("phone");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.TypeService)
                    .HasMaxLength(40)
                    .HasColumnName("type_service");

                entity.Property(e => e.Voucher)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("voucher");
                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.VoucherBy).HasColumnName("voucher_by");
                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Bills)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("Bill_Admin_null_fk");
            });

            modelBuilder.Entity<Spending>(entity =>
            {
                entity.ToTable("Spending");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Content)
                    .HasMaxLength(100)
                    .HasColumnName("content");

                entity.Property(e => e.CreateBy).HasColumnName("create_by");

                entity.Property(e => e.CreateDay)
                    .HasColumnType("datetime")
                    .HasColumnName("create_day");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.HasOne(d => d.CreateByNavigation)
                    .WithMany(p => p.Spendings)
                    .HasForeignKey(d => d.CreateBy)
                    .HasConstraintName("Spending_Admin_null_fk");
            });
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
