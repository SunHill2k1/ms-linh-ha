using AutoMapper;
using GardenUTH.Entities;

namespace GardenUTH.AutoMapper;

public class AutoMapperProfile: Profile
{
    public AutoMapperProfile()
    {
        CreateMap<Voucher, Voucher>()
            .ForMember(dest => dest.CreateByNavigation, opt =>
                opt.MapFrom(src => new Admin { Id = src.CreateByNavigation.Id, Name = src.CreateByNavigation.Name }));
        CreateMap<Spending, Spending>()
            .ForMember(dest => dest.CreateByNavigation, opt =>
                opt.MapFrom(src => new Admin { Id = src.CreateByNavigation.Id, Name = src.CreateByNavigation.Name, Belong = src.CreateByNavigation.Belong }));
    }
}