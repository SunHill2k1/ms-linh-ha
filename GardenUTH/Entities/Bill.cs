﻿namespace GardenUTH.Entities
{
    public partial class Bill
    {
        public int Id { get; set; }
        public string? Voucher { get; set; }
        public string? CustomerName { get; set; }
        public string? Phone { get; set; }
        public DateTime DateUsed { get; set; }
        public string? TypeService { get; set; }
        public int? CodeKtv { get; set; }
        public int? CustomerGroup { get; set; }
        public bool? CustomerOdd { get; set; }
        public bool? CustomerVoucher { get; set; } = false;

        public int? NotedVoucher { get; set; }
        public string? NotedCustomer { get; set; }
        public string? Room { get; set; }
        // public DateTime? CreateDate { get; set; }
        public int? VoucherBy { get; set; }
        public double Price { get; set; }
        public int? CreateBy { get; set; }

        public virtual Admin? CreateByNavigation { get; set; }
    }
}
