﻿using GardenUTH.Entities;

namespace GardenUTH.Entities
{
    public partial class Admin
    {
        public Admin()
        {
            Vouchers = new HashSet<Voucher>();
            Bills = new HashSet<Bill>();
            Spendings = new HashSet<Spending>();
        }

        public int Id { get; set; }
        public string? Name { get; set; }
        public string? SDT { get; set; }
        public string? Belong { get; set; }
        public string Role { get; set; }
        public string? Account { get; set; }
        public string? Password { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? Shift { get; set; } = true;
        public virtual ICollection<Voucher> Vouchers { get; set; }
        public virtual ICollection<Bill> Bills { get; set; }
        public virtual ICollection<Spending> Spendings { get; set; }

    }
}
