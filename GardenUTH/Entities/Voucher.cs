﻿namespace GardenUTH.Entities
{
    public partial class Voucher
    {
        public int Id { get; set; }
        public string? Value { get; set; }
        // public int? Quantity { get; set; }
        // public DateTime? Expired { get; set; }
        public string? Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }

        public virtual Admin? CreateByNavigation { get; set; }
    }
    public partial class VoucherResponse:Voucher
    {
        
        public string? CreateName { get; set; }
       
    }
}
