﻿namespace GardenUTH.Entities
{
    public partial class Spending
    {
        public int Id { get; set; }
        public double? Price { get; set; }
        public string? Content { get; set; }
        public DateTime CreateDay { get; set; }
        public int? CreateBy { get; set; }

        public virtual Admin? CreateByNavigation { get; set; }
    }
}
