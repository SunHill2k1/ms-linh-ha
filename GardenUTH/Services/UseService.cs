using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using GardenUTH.Context;
using GardenUTH.Entities;
using GardenUTH.Models.ModelDto;
using GardenUTH.Models.ModelExtra;

using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;


namespace GardenUTH.Services
{
    public interface IUserService
    {
        Task<LoginViewModel> Login(UserLogin request);
       // Task<LoginViewModel> RefreshToken(TokenApiModel tokenApiModel);

        Task<PageResult<Admin>> GetAllUser(PagingUserRequest request);
        Task<Admin> UpdateUser(UserUpdate request);
        Task<Admin> UpdateProfile(UserUpdate request);
        Task<string> RemoveUser(int id);

        //Task<PageResult<User>> GetAllShipper(PagingUserRequest request);
       Task<LoginViewModel> GetInforUser();
        int GetUserId();
        string GetUserBelong();

        Task<object> PostUserInfo(Admin userInfo);
        //Task<object> PostUserAdmin(UserRegisterAdmin userInfo);

        Task<Admin> ChangePassword(ChangePassViewModel request);
      
    }

    public class UserService : IUserService
    {
        private readonly MyDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _http;
        //private readonly IEmailSender _emailSender;


        public UserService(MyDbContext context, IConfiguration configuration, IHttpContextAccessor http
            )
        {
            _configuration = configuration;
            _http = http;
            _context = context;
            //_emailSender = emailSender;
        }

        public Task<PageResult<Admin>> GetAllUser(PagingUserRequest request)
        {
            var nextPage = _context.Admins
                .Where(b => (request.Role == null  || request.Role.Contains((b.Role!))) &&
                    (request.Belong == null || request.Belong.Contains((b.Belong!))) &&
                            (request.Search == null || b.Name.Contains(request.Search))  &&
                                (b.IsDeleted == false) )
                .OrderBy(request.OrderBy!, request.AES == true ? "" : "Descending");
            var res = nextPage
                .Skip(((int)request.PageIndex!) * (int)request.PageSize!)
                .Take((int)request.PageSize)
                .ToList();
            return Task.FromResult(new PageResult<Admin>()
            {
                TotalRecord = nextPage.Count(),
                PageSize=request.PageSize,
                PageIndex=request.PageIndex,
                TotalPages=nextPage.Count()/request.PageSize +1,
                Data = res
            });
        }

        public async Task<Admin> UpdateUser(UserUpdate request)
        {
            var userDB =await _context.Admins.FirstOrDefaultAsync(x=>x.Id== request.Id);
            userDB.Name = request.Name??userDB.Name;
            userDB.SDT = request.SDT??userDB.SDT;
            userDB.Belong = request.Belong??userDB.Belong;
            userDB.Role = request.Role??userDB.Role;
            userDB.Shift = request.Shift??userDB.Shift;
            
            _context.Entry(userDB).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                return userDB;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public async Task<Admin> UpdateProfile(UserUpdate request)
        {
            var userDB =await _context.Admins.FindAsync( GetUserId());
            userDB.Name = request.Name??userDB.Name;
            userDB.SDT = request.SDT??userDB.SDT;
            userDB.Belong = request.Belong??userDB.Belong;
            userDB.Role = request.Role??userDB.Role;
            
            _context.Entry(userDB).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                return userDB;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public async Task<string> RemoveUser(int id)
        {
            var userDB =await _context.Admins.FirstOrDefaultAsync(x=>x.Id== id);
            userDB.IsDeleted = true;
            _context.Entry(userDB).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
                return "Xóa User thành công";
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int GetUserId()
        {
            var id = _http.HttpContext.User.Claims.First(i => i.Type.ToLower() == "iduser").Value;
            return Convert.ToInt32(id);
        }
        
        public string GetUserBelong()
        {
            var belong = _http.HttpContext.User.Claims.First(i => i.Type == "Belong").Value;
            return belong;
        }


    //     public async Task<LoginViewModel> RefreshToken(TokenApiModel tokenApiModel)
    //     {
    //         try
    //         {
    //             string accessToken = tokenApiModel.AccessToken;
    //             string refreshToken = tokenApiModel.RefreshToken;
    //             var principal = GetPrincipalFromExpiredToken(accessToken);
    //             var idUser = principal.Claims.First(i => i.Type.ToLower() == "iduser").Value;
    //             var user = await _context.Users.FirstOrDefaultAsync(x => x.IdUser == Convert.ToInt32(idUser));
    //             if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
    //                 throw new Exception("Token không chính xác");
    //             return new LoginViewModel()
    //             {
    //                 Token = GenerateAccessToken(principal.Claims, 60),
    //                 TokenType = "Bearer",
    //                 Fullname = user.FullName,
    //                 IdUser = user.IdUser,
    //                 Role = user.Role,
    //                 Email = user.Email,
    //                 Address = user.Address,
    //                 PhoneNumber = user.PhoneNumber
    //             };
    //         }
    //         catch (Exception e)
    //         {
    //             throw e;
    //         }
    //     }
    //
    private string GenerateAccessToken(IEnumerable<Claim> claims, int tokenExpiryTime)
    {
        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        var expire = DateTime.Now.AddYears(tokenExpiryTime);
        var token =
            new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Issuer"],
                claims,
                expires: expire,
                signingCredentials: credentials
            );
        return new JwtSecurityTokenHandler().WriteToken(token);
    }
    
    
        public async Task<object> PostUserInfo(Admin userInfo)
        {
            try
            {
                if (userInfo.Role != "Admin" || userInfo.Role != "Tổng quản lý chi nhánh")
                {
                    if (string.IsNullOrEmpty(userInfo.Belong)) throw new Exception("Vui lòng chọn chi nhánh cho người dùng");
                }
                else if (!string.IsNullOrEmpty(userInfo.Belong)) throw new Exception("Chức danh này không cần chọn chi nhánh");
                
                
                var user =
                    await _context.Admins.FirstOrDefaultAsync(x => x.Account == userInfo.Account);
                if (user != null)
                    throw new Exception("Tên tài khoản đã tồn tại, xin vui lòng thử tên khác");

                var userDb = new Admin()
                {
                    Account = userInfo.Account,
                    Name = userInfo.Name,
                    Password = BCrypt.Net.BCrypt.HashPassword(userInfo.Password),
                    Belong = userInfo.Belong,
                    Role = userInfo.Role,
                    IsDeleted = false,
                    Shift = userInfo.Shift
                    
                };
                    _context.Admins.Add(userDb);
                
            await _context.SaveChangesAsync();
            return userDb;
        }
        catch (Exception e)
        {
            throw e;
        }
    }
    //
    // public async Task<object> PostUserAdmin(UserRegisterAdmin userInfo)
    // {
    //     try
    //     {
    //         var user = _context.Users.Any(x => x.Name == userInfo.Name);
    //         if (user)
    //             throw new Exception("Tên tài khoản đã tồn tại, xin vui lòng thử tên khác");
    //         var userDB = new User()
    //         {
    //             Name = userInfo.Name,
    //             Address = userInfo.Address,
    //             Password = BCrypt.Net.BCrypt.HashPassword(userInfo.Password),
    //             FullName = userInfo.FullName,
    //             Email = userInfo.Email,
    //             PhoneNumber = userInfo.PhoneNumber,
    //             Role = userInfo.Role
    //         };
    //         _context.Users.Add(userDB);
    //         await _context.SaveChangesAsync();
    //         return userInfo;
    //     }
    //     catch (Exception e)
    //     {
    //         throw e;
    //     }
    // }
    //
    public async Task<Admin> ChangePassword(ChangePassViewModel request)
    {
        try
        {
            if (request.NewPassword != request.ConfirmNewPassword)
                throw new Exception("Xác nhận mật khẩu không chính xác");
            int currentIdUser = GetUserId();
            var user = await _context.Admins.FirstOrDefaultAsync(x => x.Id.Equals(currentIdUser));
            if (!BCrypt.Net.BCrypt.Verify(request.OldPassword, user.Password))
                throw new Exception("Mật khẩu cũ không chính xác");
            user.Password = BCrypt.Net.BCrypt.HashPassword(request.NewPassword);
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return user;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public async Task<Admin> UpdateRole(int idUser, string newRole)
    {
        try
        {
            var UserDB = await _context.Admins.FindAsync(idUser);
            UserDB.Role = newRole;
            _context.Entry(UserDB).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return UserDB;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    // public async Task<string> ResetPassword(string name, string? email)
    // {
    //     try
    //     {
    //         var user = await _context.Users.FirstOrDefaultAsync(x => x.Name == name);
    //
    //         if (user == null)
    //         {
    //             throw new Exception("Tài khoản không tồn tại, vui lòng thử lại");
    //         }
    //
    //         ;
    //         if (user.Email != email)
    //         {
    //             var index = user.Email.IndexOf("@");
    //
    //             string myString = new string('*', index - 3);
    //             throw new Exception(
    //                 $"Email của tài khoản '{name}' không chính xác, nó phải  là: {user.Email.Substring(0, 3)}{myString}{user.Email.Substring(index - 1)}");
    //         }
    //
    //         var randomNumber = new byte[16];
    //         using (var rng = RandomNumberGenerator.Create())
    //         {
    //             rng.GetBytes(randomNumber);
    //         }
    //
    //         var newPassword = Convert.ToBase64String(randomNumber);
    //         user.Password = BCrypt.Net.BCrypt.HashPassword(newPassword);
    //         _context.Entry(user).State = EntityState.Modified;
    //         await _context.SaveChangesAsync();
    //         var message = new Message(new string[] { email },
    //             "GardenUTH xin gửi lại mật khẩu cho quý khách, chúc quý khách một ngày mua sắm vui vẻ",
    //             $"Mật khẩu mới của tài khoản {name} là : {newPassword}");
    //         _emailSender.SendEmail(message);
    //         return "Đã gửi mật khẩu mới về email của bạn, vui lòng kiểm tra";
    //     }
    //
    //     catch (Exception e)
    //     {
    //         throw e;
    //     }
    // }

    // public async Task<string> ConfirmEmailRegister(string userName, int num)
    // {
    //     var user = await _context.Users.FirstOrDefaultAsync(x => x.Name == userName);
    //     // if (user.ExpiryTimeCode < DateTime.Now)
    //     //     throw new Exception("Đã hết thời gian xác nhận, vui lòng yêu cầu gửi lại mã xác thực");
    //     if (user.CodeActive == num)
    //     {
    //         user.CodeActive = null;
    //         user.ExpiryTimeCode = null;
    //         _context.Entry(user).State = EntityState.Modified;
    //
    //         await _context.SaveChangesAsync();
    //
    //         return ("Xác nhận tài khoản thành công");
    //     }
    //
    //     return ("Mã xác nhận không chính xác");
    // }
    //
    // public async Task<string> RegisterEmailAgain(string userName)
    // {
    //     try
    //     {
    //         var userInfo = await _context.Users.FirstOrDefaultAsync(x => x.Name == userName);
    //         if (userInfo.CodeActive == null) throw new Exception("Lỗi cú pháp");
    //         Random _rdm = new Random();
    //         var num = _rdm.Next(1000, 999999);
    //         userInfo.CodeActive = num;
    //         userInfo.ExpiryTimeCode = DateTime.Now.AddSeconds(45);
    //         _context.Entry(userInfo).State = EntityState.Modified;
    //         await _context.SaveChangesAsync();
    //         var message = new Message(new string[] { userInfo.Email }, "GardenUTH xin gửi lại mã xác thực tài khoản",
    //             $"Mã xác thực của tài khoản {userInfo.Name} là : {num}");
    //         _emailSender.SendEmail(message);
    //         return "Đã gửi mã xác thực thành công, vui lòng kiểm tra lại email";
    //     }
    //     catch (Exception e)
    //     {
    //         throw e;
    //     }
    // }

    public async Task<string> SendSMS(string number)
    {
        string messMeta = "ma cua ban la 7655 rat vui khi duoc hop tac voiw ban";
        String message = HttpUtility.UrlEncode(messMeta);
        using (var wb = new WebClient())
        {
            byte[] response = wb.UploadValues("https://api.txtlocal.com/send/", new NameValueCollection()
            {
                { "apikey", "NTY0ZDQ2NTM1NTZkNTIzNjYxNjQzMzRlMzU1NjQ3Njk=" },
                { "numbers", number },
                { "message", message },
                { "sender", "GardenUTH" }
            });
            string result = System.Text.Encoding.UTF8.GetString(response);
            return result;
        }
    }

    public async Task<LoginViewModel> Login(UserLogin request)
    {
        try
        {
            var user = await _context.Admins.FirstOrDefaultAsync(x => x.Account==request.name);
            if (user == null || user.IsDeleted == true )
            {
                throw new Exception($"Tài khoản không tồn tại");
            }
            //
            // if (user.CodeActive != null)
            // {
            //     throw new Exception(
            //         $"Tài khoản này chưa được kích hoạt bởi email, vui lòng kích hoạt bằng cách kiểm tra email");
            // }

            if (!BCrypt.Net.BCrypt.Verify(request.password, user.Password))
            {
                throw new Exception($"Mật khẩu không chính xác");
            }

            #region Generate token & information

            var claims = new[]
            {
                new Claim(AuthConstants.ID, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Name!),
                new Claim(ClaimTypes.Role, user.Role!),
                new Claim(ClaimTypes.MobilePhone, (user.SDT??null) ?? string.Empty),
                new Claim(AuthConstants.BELONG, user.Belong!),

            };

            #endregion

            // var refreshToken = GenerateRefreshToken();
            // user.RefreshToken = refreshToken;
            // user.RefreshTokenExpiryTime = DateTime.Now.AddMinutes(60);
            // _context.Entry(user).State = EntityState.Modified;
            // await _context.SaveChangesAsync();
            return new LoginViewModel()
            {
                Token = GenerateAccessToken(claims, 10),
                TokenType = "Bearer",
                Fullname = user.Name,
                IdUser = user.Id,
                Role = user.Role,
                Belong=user.Belong,
                SDT= user.SDT,
                Shift=user.Shift
            };
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public async Task<LoginViewModel> GetInforUser()
    {
        var user = await _context.Admins.FindAsync(GetUserId());
        // var fullname = _http.HttpContext.User.FindFirstValue(ClaimTypes.Name);
        // var role = _http.HttpContext.User.FindFirstValue(ClaimTypes.Role);
        // var phone = _http.HttpContext.User.FindFirstValue(ClaimTypes.MobilePhone);
        //var id = _http.HttpContext.User.Claims.First(i => i.Type.ToLower() == "iduser").Value;
        return new LoginViewModel()
        {
            Fullname = user.Name,
            IdUser = GetUserId(),
            Role = user.Role,
            Belong=GetUserBelong(),
            SDT= user.SDT,
            Shift=user.Shift
        };
    }

    private string GenerateRefreshToken()
    {
        var randomNumber = new byte[32];
        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }
    }

    private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience =
                false, //you might want to validate the audience and issuer depending on your use case
            ValidateIssuer = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"])),
            ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
        };
        var tokenHandler = new JwtSecurityTokenHandler();
        SecurityToken securityToken;
        var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
        var jwtSecurityToken = securityToken as JwtSecurityToken;
        if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                StringComparison.InvariantCultureIgnoreCase))
            throw new SecurityTokenException("Token không hợp lệ");
        return principal;
    }
}

}