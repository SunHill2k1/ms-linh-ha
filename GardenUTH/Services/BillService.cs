
using GardenUTH.Context;
using GardenUTH.Entities;
using GardenUTH.Models.ModelDto;
using GardenUTH.Models.ModelExtra;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math;


namespace GardenUTH.Services
{
    public interface IBillService
    {
        Task<Admin> PostBill(Bill request);
        Task<PageResult<BIllResponse>> GetBill(PagingBillRequest request);
        Task<object> Income(IncomeRequest request);


    }

    public class BillService : IBillService
    {
        private readonly MyDbContext _context;
        private readonly IUserService _userService;



        public BillService(MyDbContext context, IUserService userService )
        {
            _userService = userService;
            // _http = http;
            _context = context;
        }


        // public async Task<Bill> PostBill(Bill request)
        // {
        //     throw new NotImplementedException();
        // }

        public async Task<PageResult<BIllResponse>> GetBill(PagingBillRequest request)
        {
            var nextPage = _context.Bills
                .Join(
                    _context.Admins,
                    bill => bill.CreateBy,
                    admin => admin.Id,
                    (bill, admin) => new { Bills = bill, Admin = admin }
                )
                .Where(b => (request.CreateBy == null || request.CreateBy.Contains((b.Bills.CreateBy!))) &&
                            (request.CustomerName == null || request.CustomerName.Contains((b.Bills.CustomerName!))) &&
                            (request.Search == null || b.Bills.CustomerName!.Contains(request.Search)))
                .Select(group => new BIllResponse
                {
                     Bill=group.Bills,
                    Belong = group.Admin.Belong

                });
               // .OrderBy(request.OrderBy!, request.AES == true ? "" : "Descending")123;
                
            var res = nextPage
                .Skip(((int)request.PageIndex!) * (int)request.PageSize!)
                .Take((int)request.PageSize)
                .ToList();
            return new PageResult<BIllResponse>()
            {
                TotalRecord = nextPage.Count(),
                PageSize=request.PageSize,
                PageIndex=request.PageIndex,
                TotalPages=nextPage.Count()/request.PageSize +1,
                Data = res
            };
        }

        public async Task<object> Income(IncomeRequest request)
        {

            var dailySum = _context.Bills
                .Where(x => request.Shift == null ||
                            (request.Shift == true && x.DateUsed.Hour >= 8 && x.DateUsed.Hour < 20) ||
                            (request.Shift == false &&
                             (x.DateUsed.Hour >= 20 || (x.DateUsed.Hour >= 0 && x.DateUsed.Hour < 8))))
                .Where(x => request.From == null || x.DateUsed >= request.From)
                .Where(x => request.To == null || x.DateUsed.Date.AddDays(-1) <= request.To)
                .Where(x => request.Belong == null || x.CreateByNavigation!.Belong == request.Belong )
                .GroupBy(x => x.DateUsed.Date)
                .Select(g => new
                {
                    Date = g.Key,
                    TotalPrice = g.Sum(x => x.Price)
                });
            return dailySum;
        }

        public async Task<Admin> PostBill(Bill request)
        {
        if (request.CustomerGroup != 0 && request.CustomerOdd == true ) throw new Exception("Không được chọn cùng lúc 'Khách nhóm' và 'Khách lẻ'");
        if (request.CustomerGroup == 0 && request.CustomerOdd == false ) throw new Exception("Vui lòng chọn đây là 'Khách nhóm' hay 'Khách lẻ'");
            var bill = new Bill();
            bill = request;
            bill.CreateBy = _userService.GetUserId();
            bill.DateUsed=DateTime.Now;
            var staff= new Admin();
            try
            {
                if (!string.IsNullOrEmpty(request.Voucher))
                {
                    var voucher = await _context.Vouchers.FirstOrDefaultAsync(x => x.Value == request.Voucher)
                                  ?? throw new Exception("Voucher không tồn tại");;
                       
                    if (voucher.Status=="đã sử dụng")
                        throw new Exception("Voucher đã được sử dụng");
                    bill.VoucherBy = voucher.CreateBy;
                    voucher.Status = "đã sử dụng";
                    staff = await _context.Admins.FirstOrDefaultAsync(x => x.Id == voucher.CreateBy);
                }
                //else if (request.NotedVoucher != 0) throw new Exception("Bạn chưa nhập voucher, hoặc bỏ trống trường 'Ghi chú khuyến mãi'");

                _context.Bills.Add(bill);
                
            await _context.SaveChangesAsync();
            return staff;
        }
        catch (Exception e)
        {
            throw e;
        }
    }
    
}

}