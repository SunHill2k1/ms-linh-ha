
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GardenUTH.Context;
using GardenUTH.Entities;
using GardenUTH.Models.ModelDto;
using GardenUTH.Models.ModelExtra;
using Microsoft.EntityFrameworkCore;


namespace GardenUTH.Services
{
    public interface ISpendingServer
    {
        Task<Spending> PostSpending(Spending request);
        Task<PageResult<Spending>> GetSpending(PagingSpendingRequest request);
        Task<object> Spending(IncomeRequest request);
    }

    public class SpendingServer : ISpendingServer
    {
        private readonly MyDbContext _context;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public SpendingServer(MyDbContext context, IUserService userService, IMapper mapper )
        {
            _userService = userService;
            _context = context;
            _mapper = mapper;
        }

        public async Task<PageResult<Spending>> GetSpending(PagingSpendingRequest request)
        {
            var nextPage = _context.Spendings
                .Include(x=> x.CreateByNavigation)
                .Where(x => request.Search == null || x.Content!.Contains(request.Search)  )
                .OrderBy(x=> x.CreateDay)
                .ProjectTo<Spending>(_mapper.ConfigurationProvider, x => x.CreateByNavigation);
                
            var res = nextPage
                .Skip(((int)request.PageIndex!) * (int)request.PageSize!)
                .Take((int)request.PageSize)
                .ToList();
            return new PageResult<Spending>()
            {
                TotalRecord = nextPage.Count(),
                PageSize=request.PageSize,
                PageIndex=request.PageIndex,
                TotalPages=nextPage.Count()/request.PageSize +1,
                Data = res
            };
        }

        public async Task<object> Spending(IncomeRequest request)
        {

            var dailySum = _context.Spendings
                .Where(x => request.Shift == null ||
                            (request.Shift == true && x.CreateDay.Hour >= 8 && x.CreateDay.Hour < 20) ||
                            (request.Shift == false &&
                             (x.CreateDay.Hour >= 20 || (x.CreateDay.Hour >= 0 && x.CreateDay.Hour < 8))))
                .Where(x => request.From == null || x.CreateDay >= request.From)
                .Where(x => request.To == null || x.CreateDay.Date.AddDays(-1) <= request.To)
                .Where(x => request.Belong == null || x.CreateByNavigation!.Belong == request.Belong )
                .GroupBy(x => x.CreateDay.Date)
                .Select(g => new
                {
                    Date = g.Key,
                    TotalPrice = g.Sum(x => x.Price)
                });
            return dailySum;
        }

        public async Task<Spending> PostSpending(Spending request)
        {
        // if (request.CustomerGroup != 0 && request.CustomerOdd == true ) throw new Exception("Không được chọn cùng lúc 'Khách nhóm' và 'Khách lẻ'");
        // if (request.CustomerGroup == 0 && request.CustomerOdd == false ) throw new Exception("Vui lòng chọn đây là 'Khách nhóm' hay 'Khách lẻ'");
            var spending = new Spending();
            spending = request;
            spending.CreateDay=DateTime.Now;
            spending.CreateBy = _userService.GetUserId();
            try
            {
                _context.Spendings.Add(spending);
                await _context.SaveChangesAsync();
                return spending;
            }
            catch (Exception e)
            {
                throw e;
            }
         }
    
    }

}