﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 8888

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["GardenUTH/GardenUTH.csproj", "GardenUTH/"]
RUN dotnet restore "GardenUTH/GardenUTH.csproj"
COPY . .
WORKDIR "/src/GardenUTH"
RUN dotnet build "GardenUTH.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "GardenUTH.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENV ASPNETCORE_URLS http://*:8888
ENTRYPOINT ["dotnet", "GardenUTH.dll"]
